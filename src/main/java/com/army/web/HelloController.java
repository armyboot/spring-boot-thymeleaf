package com.army.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HelloController {

    @GetMapping("/hello")
    public String index(ModelMap map) {
        map.addAttribute("message", "http://www.army16.com");
        return "hello";
    }
}

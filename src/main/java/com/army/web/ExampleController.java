package com.army.web;

import com.army.vo.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class ExampleController {

    @GetMapping("/example")
    public String example(ModelMap map) {
        map.addAttribute("userName", "HoneJun");
        map.addAttribute("flag", "yes");
        map.addAttribute("users", getUserList());
        map.addAttribute("type", "link");
        map.addAttribute("pageId","springboot/2018/08/10");
        map.addAttribute("img","https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1533906207328&di=c494b0afbeca38678f11ea89b5a24576&imgtype=0&src=http%3A%2F%2Fimg1.mydrivers.com%2Fimg%2F20170712%2F04024283401a4e33bd7c24258e376fbe.jpg");
        map.addAttribute("count",12);
        map.addAttribute("date", new Date());
        return "example";
    }

    private List<User> getUserList() {
        List<User> list = new ArrayList<User>();
        User user1 = new User("大头", 13, "12345");
        User user2 = new User("小头", 18, "22333");
        User user3 = new User("红狼", 36, "65432");
        list.add(user1);
        list.add(user2);
        list.add(user3);
        return list;
    }
}
